﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowRestorer
{
    class ScreenCountObserver
    {
        private readonly System.Timers.Timer _timer;
        private readonly object _TimerLock = new object();
        private bool _settingsChanging = false;
        private int _maxScreenCount;
        private int _currentScreenCount;

        public event Action RunningOnMaxScreens;
        public event Action ScreenConnected;
        public event Action ScreenDisconnected;

        public ScreenCountObserver()
        {
            _timer = new System.Timers.Timer();
            _timer.Interval = 2000;
            _timer.Elapsed += OnTimerElapsed;
        }

        public void Start()
        {
            _currentScreenCount = WindowsNative.GetNumberOfScreens();
            _maxScreenCount = _currentScreenCount;
            _timer.Start();
            Microsoft.Win32.SystemEvents.DisplaySettingsChanging += OnDisplaySettingsChanging;
            Microsoft.Win32.SystemEvents.DisplaySettingsChanged += OnDisplaySettingsChanged;
        }

        public void Stop()
        {
            _timer.Stop();
            Microsoft.Win32.SystemEvents.DisplaySettingsChanging -= OnDisplaySettingsChanging;
            Microsoft.Win32.SystemEvents.DisplaySettingsChanged -= OnDisplaySettingsChanged;
        }

        private void OnDisplaySettingsChanged(object sender, EventArgs e)
        {
            _settingsChanging = false;
        }

        private void OnDisplaySettingsChanging(object sender, EventArgs e)
        {
            _settingsChanging = true;
        }

        private void OnTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!_settingsChanging && Monitor.TryEnter(_TimerLock))
            {
                try
                {
                    var newScreenCount = WindowsNative.GetNumberOfScreens();
                    var oldScreenCount = _currentScreenCount;
                    _currentScreenCount = newScreenCount;

                    if (newScreenCount > oldScreenCount)
                    {
                        OnScreenConnected();
                    }
                    else if (newScreenCount < oldScreenCount)
                    {
                        OnScreenDisconnected();
                    }
                    else if (newScreenCount == _maxScreenCount)
                    {
                        OnRunningOnMaxScreens();
                    }

                    if (newScreenCount > _maxScreenCount)
                    {
                        _maxScreenCount = newScreenCount;
                    }
                }
                finally
                {
                    Monitor.Exit(_TimerLock);
                }
            }
        }

        private void OnScreenConnected()
        {
            Log.Info("Screen connected.");
            var handler = this.ScreenConnected;
            if (handler != null)
            {
                handler();
            }
        }

        private void OnScreenDisconnected()
        {
            Log.Info("Screen disconnected.");
            var handler = this.ScreenDisconnected;
            if (handler != null)
            {
                handler();
            }
        }

        private void OnRunningOnMaxScreens()
        {
            var handler = this.RunningOnMaxScreens;
            if (handler != null)
            {
                handler();
            }
        }
    }
}
