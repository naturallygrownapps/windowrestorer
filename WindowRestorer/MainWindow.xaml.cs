﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;

namespace WindowRestorer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private class ViewModel
        {
            public ObservableCollection<string> LogItems { get; } = new ObservableCollection<string>();
        }

        private readonly ViewModel _vm;
        private System.Windows.Forms.NotifyIcon _notifyIcon;
        private WindowPositioner _windowPositioner;

        public MainWindow()
        {
            InitializeComponent();

            _vm = new ViewModel();
            this.DataContext = _vm;

            _notifyIcon = new System.Windows.Forms.NotifyIcon();
            _notifyIcon.DoubleClick += OnNotifyIconDoubleClick;
            _notifyIcon.Visible = true;
            _notifyIcon.BalloonTipText = this.Title;
            StreamResourceInfo sri = Application.GetResourceStream(new Uri("Assets/notifyIcon.ico", UriKind.Relative));
            if (sri != null)
            {
                using (var s = sri.Stream)
                {
                    _notifyIcon.Icon = new System.Drawing.Icon(s);
                }
            }

            var notifyIconContextMenu = new System.Windows.Forms.ContextMenu();
            notifyIconContextMenu.MenuItems.Add("Exit", (sender, args) =>
            {
                Application.Current.Shutdown();
            });
            _notifyIcon.ContextMenu = notifyIconContextMenu;

            Application.Current.Exit += OnApplicationExit;
            Log.Message += WriteLogMessage;

            _windowPositioner = new WindowPositioner();
            _windowPositioner.Start();

            this.WindowState = WindowState.Minimized;
            this.Visibility = Visibility.Hidden;
        }

        private void OnApplicationExit(object sender, ExitEventArgs e)
        {
            _notifyIcon.Dispose();
            Log.Message -= WriteLogMessage;
            if (_windowPositioner != null)
            {
                _windowPositioner.Stop();
            }
        }

        private void WriteLogMessage(string obj)
        {
            Dispatcher.BeginInvoke((Action)(() => _vm.LogItems.Add(obj)));
        }

        private void OnNotifyIconDoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                Minimize();
            }
            else
            {
                Restore();
            }
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Minimize();
            e.Cancel = true;
        }

        private void Minimize()
        {
            this.WindowState = WindowState.Minimized;
            this.Visibility = Visibility.Hidden;
        }

        private void Restore()
        {
            this.Visibility = Visibility.Visible;
            Dispatcher.BeginInvoke((Action)(() =>
            {
                this.WindowState = WindowState.Normal;
            }));
        }
    }
}
