﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowRestorer
{
    public static class Log
    {
        public static event Action<string> Message;

        public static void Info(string msg)
        {
            Write("INFO", msg);
        }

        public static void Error(string error)
        {
            Write("ERROR", error);
        }

        public static void Write(string type, string msg)
        {
            var handler = Log.Message;
            if (handler != null)
            {
                var date = DateTime.Now;
                handler(date.ToString("G") + " " +  type + "> " + msg);
            }
        }
    }
}
