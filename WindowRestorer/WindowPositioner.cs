﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowRestorer
{
    class WindowPositioner
    {
        private readonly ScreenCountObserver _screenCountObserver;
        private List<WindowsNative.WindowInformation> _windowsToRestore;
        private List<WindowsNative.WindowInformation> _maxScreenWindows;

        public WindowPositioner()
        {
            _screenCountObserver = new ScreenCountObserver();
            _screenCountObserver.RunningOnMaxScreens += SaveWindowPositions;
            _screenCountObserver.ScreenConnected += RestoreWindowPositions;
            _screenCountObserver.ScreenDisconnected += RememberMaxScreenWindows;
        }

        public void Start()
        {
            _screenCountObserver.Start();
        }

        public void Stop()
        {
            _screenCountObserver.Stop();
        }

        private void SaveWindowPositions()
        {
            _maxScreenWindows = WindowsNative.EnumDesktopWindows();
        }

        private void RememberMaxScreenWindows()
        {
            _windowsToRestore = _maxScreenWindows;
        }

        private void RestoreWindowPositions()
        {
            if (_windowsToRestore != null)
            {
                var currentWindows = WindowsNative.EnumDesktopWindows();
                foreach (var win in currentWindows)
                {
                    var previousWinInfo = _windowsToRestore.Find(w => w.Handle == win.Handle);
                    if (previousWinInfo != null)
                    {
                        var x = previousWinInfo.Rect.Left;
                        var y = previousWinInfo.Rect.Top;
                        var width = previousWinInfo.Rect.Right - previousWinInfo.Rect.Left;
                        var height = previousWinInfo.Rect.Bottom - previousWinInfo.Rect.Top;
                        if (WindowsNative.SetWindowPosition(win.Handle, x, y, width, height))
                        {
                            Log.Info("Window restored: " + win.Text + " to (" + x + ", " + y + ", " + width + ", " + height + ")");
                        }
                        else
                        {
                            Log.Error("Window could not be restored: " + win.Text);
                        }
                    }
                }
            }   
        }
    }
}
